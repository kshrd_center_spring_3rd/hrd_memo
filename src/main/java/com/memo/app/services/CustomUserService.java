package com.memo.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.memo.app.entities.UserSecurConfig;
import com.memo.app.repo.UserSecurityDao;

@Service("customUserService")
public class CustomUserService implements UserDetailsService {

	@Autowired
	private UserSecurityDao userDaoImpl;

	public UserSecurConfig loadUserByUsername(String username) throws UsernameNotFoundException {
		return userDaoImpl.findUserByUserName(username);
	}

}
