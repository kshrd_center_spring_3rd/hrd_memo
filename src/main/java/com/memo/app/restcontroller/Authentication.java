package com.memo.app.restcontroller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping(value="authentication")
public class Authentication {
	
	@RequestMapping(value="/KALogin", method=RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> KALogin(){
		Map<String , Object> map = new HashMap<String, Object>();
		RestTemplate rest = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<String> response = rest.exchange("http://192.168.178.20:8080/KAWEBCLIENT/api/isLogin", HttpMethod.GET , entity , String.class) ;
		map.put("RESP_DATA", response.getBody());
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}
	
	public static void main(String args[]){
		RestTemplate rest = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<String> response = rest.exchange("http://192.168.178.20:8080/KAWEBCLIENT/api/isLogin", HttpMethod.GET , entity , String.class) ;
		System.out.println(response.getBody());
	}
}
